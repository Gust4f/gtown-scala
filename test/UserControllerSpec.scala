import java.util.Date

import akka.stream.Materializer
import controllers.UserController
import model.entities.User
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play._
import play.api.libs.json.{JsString, Json}
import play.api.mvc._
import play.api.test._
import play.api.test.Helpers._
import org.mockito.Mockito._
import org.mockito.Matchers._
import services.UserService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by gustaf on 2016-08-06.
  */
class UserControllerSpec extends PlaySpec with Results with MockitoSugar with OneAppPerSuite {

  implicit lazy val materializer: Materializer = app.materializer

  val BASE_URL = "/users/"

  "Create user" should {
    val email = "test@test.com"
    val service = mock[UserService]
    val controller = new UserController(service)
    val published = new Date()

    when(service create any[User]) thenReturn Future {}
    "send ok when valid JSON" in {
      val jsonBody = Json obj("email" -> email, "published" -> Json.toJson(published))

      val request = FakeRequest().withJsonBody(jsonBody)
      val result = call(controller.create(), request)
      val body = contentAsJson(result)
      val expectedJson = Json obj("status" -> "created", "message" -> JsString("user '" + email + "' created"))

      status(result) mustEqual CREATED
      body mustEqual expectedJson
    }
    "send Bad request when invalid JSON" in {
      val jsonBody = Json obj("test" -> email)

      val request = FakeRequest().withJsonBody(jsonBody)
      val result = call(controller.create(), request)

      status(result) mustEqual BAD_REQUEST
    }
  }

  "Delete user" should {
    val service = mock[UserService]
    val controller = new UserController(service)

    "send ok and row count > 0 if valid id" in {

      when(service delete 1) thenReturn Future {1}
      val request = FakeRequest()
      val result = call(controller.delete(1), request)
      val body = contentAsJson(result)
      val expectedJson = Json obj("status" -> "ok", "rows" -> 1)

      status(result) mustEqual OK
      body mustEqual expectedJson
    }
    "send not found if invalid id" in {

      when(service delete 0) thenReturn Future {0}
      val request = FakeRequest()
      val result = call(controller.delete(0), request)
      val body = contentAsJson(result)
      val expectedJson = Json obj("status" -> "not found", "message" -> "user could not be found")

      status(result) mustEqual NOT_FOUND
      body mustEqual expectedJson
    }
  }

  "Find user" should {
    val service = mock[UserService]
    val controller = new UserController(service)
    val email = "test@test.com"
    val published = new Date()

    "send ok with user as JSON if found" in {

      when(service find 1) thenReturn Future {Option(User(Option(1), email, published))}
      val request = FakeRequest()
      val result = call(controller.find(1), request)
      val body = contentAsJson(result)
      val expectedJson = Json obj("user" -> Json.obj("id" -> 1, "email" -> email, "published" -> Json.toJson(published)))

      status(result) mustEqual OK
      body mustEqual expectedJson
    }
    "send not found if not found" in {

      when(service find 0) thenReturn Future {None}
      val request = FakeRequest()
      val result = call(controller.find(0), request)
      val body = contentAsJson(result)
      val expectedJson = Json obj("status" -> "not found", "message" -> "user could not be found")

      status(result) mustEqual NOT_FOUND
      body mustEqual expectedJson
    }
  }

  "Find user by email" should {
    val service = mock[UserService]
    val controller = new UserController(service)
    val email = "test@test.com"
    val published = new Date()
    val jsonBody = Json obj("email" -> email, "published" -> Json.toJson(published))

    "send ok with user as JSON if found" in {

      when(service findByEmail email) thenReturn Future {Option(User(Option(1), email, published))}
      val request = FakeRequest().withJsonBody(jsonBody)
      val result = call(controller.findByEmail(), request)
      val body = contentAsJson(result)
      val expectedJson = Json obj("user" -> Json.obj("id" -> 1, "email" -> email, "published" -> Json.toJson(published)))

      status(result) mustEqual OK
      body mustEqual expectedJson
    }
    "send ok with user null if not found" in {

      when(service findByEmail email) thenReturn Future {None}
      val request = FakeRequest().withJsonBody(jsonBody)
      val result = call(controller.findByEmail(), request)
      val body = contentAsJson(result)
      val expectedJson = Json obj("status" -> "not found", "message" -> "user could not be found")

      status(result) mustEqual NOT_FOUND
      body mustEqual expectedJson
    }
    "send Bad request when invalid json" in {
      val jsonBody = Json obj("test" -> email)

      val request = FakeRequest().withJsonBody(jsonBody)
      val result = call(controller.findByEmail(), request)

      status(result) mustEqual BAD_REQUEST
    }
  }

  "Update user" should {
    val email = "test@test.com"
    val service = mock[UserService]
    val controller = new UserController(service)
    val published = new Date()

    "send ok and row count > 0 if valid JSON for existing user" in {
      when(service update any[User]) thenReturn Future {1}
      val jsonBody = Json obj("id" -> 1,"email" -> email, "published" -> Json.toJson(published))

      val request = FakeRequest().withJsonBody(jsonBody)
      val result = call(controller.update(1), request)
      val body = contentAsJson(result)
      val expectedJson = Json obj("status" -> "updated", "message" -> JsString("user '" + email + "' updated"))

      status(result) mustEqual OK
      body mustEqual expectedJson
    }
    "send not found if valid JSON for nonexistent user" in {
      when(service update any[User]) thenReturn Future {0}
      val jsonBody = Json obj("id" -> 1,"email" -> email, "published" -> Json.toJson(published))

      val request = FakeRequest().withJsonBody(jsonBody)
      val result = call(controller.update(1), request)
      val body = contentAsJson(result)
      val expectedJson = Json obj("status" -> "not found", "message" -> "user could not be found")

      status(result) mustEqual NOT_FOUND
      body mustEqual expectedJson
    }
    "send Bad request when invalid JSON" in {
      val jsonBody = Json obj("test" -> email)

      val request = FakeRequest().withJsonBody(jsonBody)
      val result = call(controller.update(1), request)

      status(result) mustEqual BAD_REQUEST
    }
  }

  "Find all users" should {
    implicit val userFormat = Json.format[User]

    val email = "test@test.com"
    val service = mock[UserService]
    val controller = new UserController(service)

    "send ok with all users in JSON array if not empty" in {
      val users = Seq(
        User(Option(1), email, new Date()),
        User(Option(2), email, new Date())
      )

      when(service findAll) thenReturn Future {users}
      val request = FakeRequest()
      val result = call(controller.findAll(), request)
      val body = contentAsJson(result)
      val expectedJson = Json obj("users" -> Json.toJson(users))

      status(result) mustEqual OK
      body mustEqual expectedJson
    }
    "send ok with empty users JSON array if none found" in {

      when(service findAll) thenReturn Future {Seq()}
      val request = FakeRequest()
      val result = call(controller.findAll(), request)
      val body = contentAsJson(result)
      val expectedJson = Json obj("users" -> Json.arr())

      status(result) mustEqual OK
      body mustEqual expectedJson
    }
  }
}

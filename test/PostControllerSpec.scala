import java.util.{Calendar, Date}

import akka.stream.Materializer
import controllers.PostController
import model.entities.Post
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.{OneAppPerSuite, PlaySpec}
import play.api.mvc.Results
import services.PostService
import org.mockito.Mockito._
import org.mockito.Matchers._
import play.api.libs.json.{JsArray, JsNull, Json}
import play.api.test.FakeRequest
import play.api.test.Helpers._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by gustaf on 2016-08-06.
  */
class PostControllerSpec extends PlaySpec with Results with MockitoSugar with OneAppPerSuite {

  implicit lazy val materializer: Materializer = app.materializer

  val date = Calendar.getInstance()
  val header = "Test!"
  val body = "This is a body"
  val footer = "footer"
  val authorId = 1
  val url = "/posts/post/1"

  "Create post" should {
    val service = mock[PostService]
    val controller = new PostController(service)
    val published = new Date()

    when(service create any[Post]) thenReturn Future {}
    "send ok when valid JSON" in {
      val jsonBody = Json obj(
        "id" -> 1,
        "header" -> header,
        "body" -> body,
        "footer" -> footer,
        "authorId" -> authorId,
        "url" -> url,
        "published" -> Json.toJson(published))

      val request = FakeRequest().withJsonBody(jsonBody)
      val result = call(controller.create(), request)
      val responseBody = contentAsJson(result)
      val expectedJson = Json obj("status" -> "created", "message" -> "post created")

      status(result) mustEqual CREATED
      responseBody mustEqual expectedJson
    }
    "send Bad request when invalid JSON" in {
      val jsonBody = Json obj(
        "id" -> 1,
        "heade" -> header,
        "body" -> body,
        "footer" -> footer,
        "authorId" -> authorId,
        "url" -> url,
        "published" -> Json.toJson(published))
      val request = FakeRequest().withJsonBody(jsonBody)
      val result = call(controller.create(), request)

      status(result) mustEqual BAD_REQUEST
    }
  }

  "Find post" should {
    val service = mock[PostService]
    val controller = new PostController(service)
    val email = "test@test.com"
    val published = new Date()

    "send Ok with post as JSON if found" in {
      when(service find 1) thenReturn Future {
        Option(Post(Option(1), published, header, body, footer, url, authorId), email)
      }
      val request = FakeRequest()
      val result = call(controller.find(1), request)
      val responseBody = contentAsJson(result)
      val expectedJson = Json obj (
        "post" -> Json.obj(
          "id" -> 1,
          "header" -> header,
          "body" -> body,
          "footer" -> footer,
          "authorId" -> authorId,
          "url" -> url,
          "published" -> Json.toJson(published)),
        "author" -> email
        )

      status(result) mustEqual OK
      responseBody mustEqual expectedJson
    }
    "send Ok with post null if not found" in {
      when(service find 0) thenReturn Future {None}
      val request = FakeRequest()
      val result = call(controller.find(0), request)
      val body = contentAsJson(result)
      val expectedJson = Json obj("status" -> "not found", "message" -> "post could not be found")

      status(result) mustEqual NOT_FOUND
      body mustEqual expectedJson
    }
  }

  "Find all posts" should {
    val service = mock[PostService]
    val controller = new PostController(service)
    val email = "test@test.com"
    val published = new Date()
    val posts = Seq(
      (Post(Option(1), published, header, body, footer, url, authorId), email),
      (Post(Option(2), published, header, body, footer, url, authorId), email)
    )
    "send Ok with all posts in JSON array if not empty" in {

      val expectedJson = Json obj( "posts" -> Seq(
        Json.obj (
          "post" -> Json.obj(
            "id" -> 1,
            "header" -> header,
            "body" -> body,
            "footer" -> footer,
            "authorId" -> authorId,
            "url" -> url,
            "published" -> Json.toJson(published)),
          "author" -> email
        ),
        Json.obj (
          "post" -> Json.obj(
            "id" -> 2,
            "header" -> header,
            "body" -> body,
            "footer" -> footer,
            "authorId" -> authorId,
            "url" -> url,
            "published" -> Json.toJson(published)),
          "author" -> email
        )
      ))

      when(service findAll) thenReturn Future {posts}
      val request = FakeRequest()
      val result = call(controller.findAll(), request)
      val responseBody = contentAsJson(result)

      status(result) mustEqual OK
      responseBody mustEqual expectedJson

    }
    "send Ok with empty JSON array if none found" in {
      when(service findAll) thenReturn Future {Seq()}
      val request = FakeRequest()
      val result = call(controller.findAll(), request)
      val responseBody = contentAsJson(result)
      val expectedJson = Json obj("posts" -> Json.arr())

      status(result) mustEqual OK
      responseBody mustEqual expectedJson
    }
  }

  "Update post" should {
    val service = mock[PostService]
    val controller = new PostController(service)
    val email = "test@test.com"
    val published = new Date()

    "send Ok when valid JSON" in {
      when(service update any[Post]) thenReturn Future {1}
      val jsonBody = Json obj(
        "id" -> 1,
        "header" -> header,
        "body" -> body,
        "footer" -> footer,
        "authorId" -> authorId,
        "url" -> url,
        "published" -> Json.toJson(published))

      val request = FakeRequest().withJsonBody(jsonBody)
      val result = call(controller.update(1), request)
      val responseBody = contentAsJson(result)
      val expectedJson = Json obj("status" -> "updated", "message" -> "post updated")

      status(result) mustEqual OK
      responseBody mustEqual expectedJson
    }
    "send Bad request when invalid JSON" in {
      when(service update any[Post]) thenReturn Future {0}
      val jsonBody = Json obj(
        "id" -> 1,
        "header" -> header,
        "body" -> body,
        "footer" -> footer,
        "authorId" -> authorId,
        "url" -> url,
        "published" -> Json.toJson(published))

      val request = FakeRequest().withJsonBody(jsonBody)
      val result = call(controller.update(1), request)
      val responseBody = contentAsJson(result)
      val expectedJson = Json obj("status" -> "not found", "message" -> "post could not be found")

      status(result) mustEqual NOT_FOUND
      responseBody mustEqual expectedJson
    }
  }

  "Delete Post" should {
    val service = mock[PostService]
    val controller = new PostController(service)

    "send Ok and row count > 0 if valid id" in {
      when(service delete 1) thenReturn Future {1}
      val request = FakeRequest()
      val result = call(controller.delete(1), request)
      val responseBody = contentAsJson(result)
      val expectedJson = Json obj("status" -> "ok", "rows" -> 1)

      status(result) mustEqual OK
      responseBody mustEqual expectedJson
    }
    "send not found if invalid id" in {
      when(service delete 0) thenReturn Future {0}
      val request = FakeRequest()
      val result = call(controller.delete(0), request)
      val body = contentAsJson(result)
      val expectedJson = Json obj("status" -> "not found", "message" -> "post could not be found")

      status(result) mustEqual NOT_FOUND
      body mustEqual expectedJson
    }
  }
}

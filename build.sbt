import WebJs._
import RjsKeys._

name := """gtown-scala"""
version := "1.0-SNAPSHOT"
packageName := "gtown"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  cache,
  ws,
  filters,
  "org.webjars" % "jquery" % "3.1.1",
  "org.webjars" % "bootstrap" % "3.3.7" exclude("org.webjars", "jquery"),
  "org.webjars.bower" % "angularjs" % "1.5.8" exclude("org.webjars", "jquery"),
  "org.webjars.bower" % "angular-route" % "1.5.8" exclude("org.webjars.bower", "angularjs"),
  "org.webjars.bower" % "angular-sanitize" % "1.5.8" exclude("org.webjars.bower", "angularjs"),
  "org.webjars.bower" % "angular-mocks" % "1.5.8" exclude("org.webjars.bower", "angularjs"),
  "org.webjars.bower" % "spin.js" % "2.3.2" exclude("org.webjars", "jquery"),
  "org.webjars.npm" % "angular-spinner" % "0.8.0" exclude("org.webjars.bower", "angularjs"),
  "org.webjars" % "angularAMD" % "0.2.1-1" exclude("org.webjars.bower", "angularjs"),
  "org.webjars.bower" % "fullpage.js" % "2.7.9",
  "org.webjars" % "animate.css" % "3.5.2",
  "org.webjars" % "FlexSlider" % "2.2.2",
  "org.webjars" % "modernizr" % "2.8.3",
  "org.webjars" % "jquery.terminal" % "0.11.2",
  "org.webjars.bower" % "tuktuk" % "1.0.0",
  "org.webjars.npm" % "chartist" % "0.9.8",

  "mysql" % "mysql-connector-java" % "5.1.34",
  "com.github.tototoshi" %% "slick-joda-mapper" % "2.0.0",
  "com.typesafe.play" %% "play-slick" % "2.0.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "2.0.0",
  "commons-net" % "commons-net" % "3.5",
  "de.svenkubiak" % "jBCrypt" % "0.4.1",
  "org.mockito" % "mockito-all" % "1.10.19" % Test,
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

includeFilter in (Assets, LessKeys.less) := "*.less"
LessKeys.compress in Assets := true

excludeFilter in rjs := new FileFilter{
  def accept(f: File) = ".*/lib/.*".r.pattern.matcher(f.getAbsolutePath).matches
}

baseUrl := "javascripts"

paths ++= Map(
  "jquery" -> ("/assets/lib/jquery/jquery" -> "empty:"),
  "angular" -> ("/assets/lib/angularjs/angular" -> "empty:"),
  "bootstrap" -> ("/assets/lib/bootstrap/js/bootstrap" -> "empty:"),
  "angularAMD" -> ("/assets/lib/angularAMD/angularAMD" -> "empty:"),
  "angular-route" -> ("/assets/lib/angular-route/angular-route" -> "empty:"),
  "angular-sanitize" -> ("/assets/lib/angular-sanitize/angular-sanitize" -> "empty:"),
  "ngload" -> ("/assets/lib/angularAMD/ngload" -> "empty:"),
  "spin" -> ("/assets/lib/spin.js/spin" -> "empty:"),
  "angular-spinner" -> ("/assets/lib/angular-spinner/angular-spinner" -> "empty:")
)

pipelineStages := Seq(digest, gzip, rjs)


// Please feel free to find a better solution if you have time.
excludeFilter in rjs := new FileFilter{
  def accept(f: File) = ".*/lib/.*".r.pattern.matcher(f.getAbsolutePath).matches
}

modules := Seq(JS.Object("name" -> "main",
  "exclude" -> Seq(
    "index",
    "login"
  )))
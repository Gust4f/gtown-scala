# GTown scala as a Play application

GTown has been rewritten multiple times in different languages using different frameworks and databases as an educational recreational project. This time it is made with Scala and the play framework creating a fully reactive application. Instead of locking the client to the current templating language used by a certain framework I have avoided using the default templating and systems in play for the client because that would lock it down once again making the iterations and flexibility seriously lacking.

Instead the client is a standalone client written in AngularJS relying on a simple REST backend and doesn't care of the framework templating language or the backend implementation language. I'm done with locking client to frameworks because all of them just straight out suck and just introduce more third party nonsense.

## Running the application

By default play starts a netty embedded web server on port 9000

```
    bin/activator run
```

On the server we run on port 8888

-Dhttp.port=8888

If debug is needed we add this flag with a port number:

-jvm-debug 9999

## Running on the Atomic container host on Amazon

Project Atomic is a really cool project that basically gives you a stripped down system where you can host Docker containers. It is unlike any other operating system because it is made for one thing. To host containers and shouldn't really provide much functionality except that so everything you need should be provided in the containers.

To access the Atomic host we use SSH with the generated key from Amazon:

```
ssh -i "~/.ssh/amazon-gtown.pem" fedora@ec2-35-156-131-224.eu-central-1.compute.amazonaws.com
```

Now we have access to the atomic host. There are no usual fedora commands available since the Atomic host is as lean as it can be. All dependencies should be provided within the application containers. One thing we need though is the Let's Encrypt certificates.

### The application in a container

To run the web application we need Java because it is build in Scala. There is a docker file in this repo to build a docker image. This image is then pushed to my personal docker hub so I can move it effortlessly across systems. This image is then pulled down to the Amazon host I've set up and deployed. Before we can deploy the web application we need to apply some other containers for the system. One of which is the actual data storage, i.e the database. We will use a sql database this time so we use the mysql image from the docker hub. The next is Nginx. To make the configuration aspect simple I made a docker file that pulls down the Nginx and then alters the configuration to fit our needs. The third is Jenkins working on the same concept as the previous. The last is using a docker compose option and sets up an ELK stack with shield based on our configurations. 

To build an image of the application manually do the following

```
./bin/activator clean stage
docker build -t "gustaf/gtown" -f Dockerfile-gtown .
```

However there is a build step available in the build.sbt file that will do everything automatically

```
./bin/activator clean stage docker
```

To launch the containers do the following:

#### MYSQL

Using the default mysql docker image. We will use the data only holder container pattern so that we can keep the database data in one place and the running mysql server in one so that we redeploy the mysql server without affecting the data.

```
docker pull mysql
docker create -v /var/lib/mysql --name dbstore mysql /bin/true
docker run -d --name gtown-db -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=gtown --volumes-from dbstore mysql
```

#### Let's Encrypt

We need SSL and HTTPS and Let's Encrypt is an awesome option and it is free. We will use certbot to generate the certificate. We cannot run HTTP/2 because the play framework doesn't support that and NGINX cannot support it in proxy mode.

So either we let NGINX adding HTTPS but that seems like a lot of wasted resources so for now we add a JKS and enable HTTPS for the embedded Netty webserver in the play framework.

#### Jenkins - stored in own repo

Do note that with a new Jenkins deployment. I.e where ctcn-jenkinsstore with the config is fresh/empty you should run the container in interactive mode because it will show you a hash for adding the admin account. If just redeploying add -d to daemonize the lunch.

```
docker build -t gtown-jenkins -f Dockerfile-jenkins .
docker create -v /var/jenkins_home --name gtown-jenkinsstore ctcn-jenkins /bin/true
docker run --name gtown-jenkins -p 8080:8080 -v /var/run/docker.sock:/var/run/docker.sock --volumes-from gtown-jenkinsstore gtown-jenkins
```

#### GTown

We want to avoid to leave password hardcoded and on the repo so naturally you replace password with the actual password during deployments.

```
docker run -d --name gtown-scala --link gtown-db:mysql -p 80:8080 -p 443:8443 gustaf/gtown:latest -Dplay.server.https.keyStore.password=password -Dslick.dbs.default.db.password=password
```

### Using Docker hub

Docker hub is a really nice way to share/store docker images on a repository. The commands are almost exactly the same as for GIT except that you are storing docker images on your repo. It is super awesome and I've used it for over two years no problem.

#### Pushing

When you have built your docker images and maybe tested them locally with in a running container we want to push it to the repo.

```
If you are not logged in:
docker login

When you are atuhenticated:
docker push gustaf/gtown

The push refers to a repository [docker.io/gustaf/gtown]
f25c0e641744: Pushed 
80c7bc683f23: Pushed 
1ace073ee479: Mounted from library/java 
17c0201149eb: Mounted from library/java 
23eb8cc995ed: Mounted from library/java 
dc7237aeb8e9: Mounted from library/java 
edb2d61496b1: Mounted from library/java 
a5eb0fc1decb: Mounted from library/java 
b2ac5371e0f2: Mounted from library/java 
142a601d9793: Mounted from library/mysql 

```

Now the latest image from you local docker has been pushed to the repo. The repo can store previous images as well to preserve history.

#### Pulling

On the atomic host in the Amazon cloud we now just have to fetch the image and deploy it in a container. No fuss. Just works.

```
docker pull gustaf/gtown

sha256:508a9a14fc7cb568d13674c9a8b41a6cce1a32708524f89649fa8b09e13975b9: Pulling from docker.io/gustaf/gtown
6a5a5368e0c2: Pull complete 
7b9457ec39de: Pull complete 
ff18e19c2db4: Pull complete 
edd38215a2fa: Pull complete 
5b26cafa72d5: Pull complete 
f340301a2a0f: Pull complete 
e8512ca75830: Pull complete 
c76b0eeb6b4d: Pull complete 
f2c046bbd04d: Pull complete 
b717fb545d3f: Pull complete 

docker run -d --name gtown-scala --link gtown-db:mysql -p 80:8080 -p 443:8443 gustaf/gtown:latest -Dplay.server.https.keyStore.password=password -Dslick.dbs.default.db.password=password
```

All done. Super easy.

### Management through Cockpit

Cockpit is an insanely useful tool built into the Atomic system and it is running by default. The only thing we need to do is open the port on amazon for our IP and we can do all management from Cockpit on our local machine. We don't even need SSH anymore because cockpit got it all. It can do everything and handle and monitor your docker containers by default. It is insane how good it is.

```
sudo atomic run cockpit/ws

Trying docker.io/cockpit/ws:latest
Uploading blob sha256:eee19b785c06d101981d2dba694f3215dd025399e8157a4fa915e33526985d13
 0 B / 5.54 KB [---------------------------------------------------------------]
Uploading blob sha256:2bf01635e2a0f7ed3800c8cb3effc5ff46adc6b9b86f0e80743c956371efe553
 62.76 MB / 69.50 MB [===================================================>-----]
Uploading blob sha256:fc813b2010153b217b1db393382860b65f749528c3e2a956fd3196734d488762
 0 B / 13.38 KB [--------------------------------------------------------------]
Uploading blob sha256:07ea723a4787f1b33d848471e51a5927330c56bacdc5f12e5eac2ff9dfd7ed54
 100.82 MB / 101.42 MB [=======================================================]
Uploading blob sha256:a94d88741a195fda31dbd8859c2192a301862eb6db15151d1cc4c579dfa685ce
 0 B / 184 B [-----------------------------------------------------------------]

```
package model.entities


import org.joda.time.DateTime
import slick.driver.MySQLDriver.api._

case class Post(id: Option[Long], published: DateTime, header: String, body: String, footer: String, category: String, url: String, author: String)

class PostTable(tag: Tag) extends Table[Post](tag, "POSTS") {

  import com.github.tototoshi.slick.MySQLJodaSupport._

  def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)
  def published = column[DateTime]("PUBLISHED")
  def header = column[String]("HEADER")
  def body = column[String]("BODY")
  def footer = column[String]("FOOTER")
  def category = column[String]("CATEGORY")
  def url = column[String]("URL")
  def author = column[String]("AUTHOR")

  def author_fk = foreignKey("POST_AUTHOR_FK", author, TableQuery[UserTable])(_.email, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
  override def * = (id.?, published, header, body, footer, category, url, author) <>(Post.tupled, Post.unapply)
}
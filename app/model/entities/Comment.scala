package model.entities


import org.joda.time.DateTime
import slick.driver.MySQLDriver.api._


case class Comment(id: Option[Long], published: DateTime, body: String, author: String, postId: Long)

class CommentTable(tag: Tag) extends Table[Comment](tag, "COMMENTS") {

  import com.github.tototoshi.slick.MySQLJodaSupport._

  def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)
  def published = column[DateTime]("PUBLISHED")
  def body = column[String]("BODY")
  def author = column[String]("AUTHOR")
  def postId = column[Long]("POST_ID")

  def author_fk = foreignKey("COMMENT_AUTHOR_FK", author, TableQuery[UserTable])(_.email, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
  def post = foreignKey("COMMENT_POST_FK", postId, TableQuery[PostTable])(_.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
  override def * = (id.?, published, body, author, postId) <>(Comment.tupled, Comment.unapply)
}
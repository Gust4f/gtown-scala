package model.entities

import org.joda.time.DateTime
import slick.driver.MySQLDriver.api._

case class User(
                 admin: Boolean,
                 email: String,
                 passwordChangeDate: DateTime,
                 password: String,
                 registrationDate: DateTime,
                 referToken: Option[String],
                 referTokenExpiration: DateTime,
                 sessionToken: Option[String],
                 sessionTokenExpiration: DateTime)

class UserTable(tag: Tag) extends Table[User](tag, "USERS") {

  import com.github.tototoshi.slick.MySQLJodaSupport._

  def admin = column[Boolean]("ADMIN")
  def email = column[String]("EMAIL", O.PrimaryKey)
  def passwordChangeDate = column[DateTime]("PASSWORD_CHANGE_DATE")
  def password = column[String]("PASSWORD")
  def registrationDate = column[DateTime]("REG_DATE")
  def referToken = column[String]("REFER_TOKEN")
  def referTokenExpiration = column[DateTime]("REFER_TOKEN_EXPIRATION")
  def sessionToken = column[String]("SESSION_TOKEN")
  def sessionTokenExpiration = column[DateTime]("SESSION_TOKEN_EXPIRATION")

  override def * = (admin, email, passwordChangeDate, password, registrationDate, referToken.?, referTokenExpiration, sessionToken.?, sessionTokenExpiration) <>(User.tupled, User.unapply)
}
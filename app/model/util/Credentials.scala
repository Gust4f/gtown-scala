package model.util

case class Credentials(email: String, password: String)

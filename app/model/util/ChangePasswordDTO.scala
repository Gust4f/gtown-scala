package model.util

/**
  * Created by gustaf on 12/16/16.
  */
case class ChangePasswordDTO(email: String, oldPassword: String, newPassword: String);

package model.util

import model.entities.{Post, User}
import play.api.libs.json.Json

object JSONConversions {
  implicit val userWrites = Json.writes[User]
  implicit val userReads = Json.reads[User]
  implicit val credentialsReads = Json.reads[Credentials]
  implicit val ChangePasswordDTOReads = Json.reads[ChangePasswordDTO]
  implicit val postWrites = Json.writes[Post]
  implicit val postReads = Json.reads[Post]
}

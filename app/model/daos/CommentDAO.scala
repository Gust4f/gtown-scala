package model.daos

import javax.inject.Inject

import model.entities.{Comment, CommentTable}
import play.api.cache.CacheApi
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import slick.driver.JdbcProfile
import scala.concurrent.duration._

import scala.concurrent.Future

class CommentDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                           protected val cache : CacheApi) extends HasDatabaseConfigProvider[JdbcProfile]{
  import driver.api._
  private lazy val comments = TableQuery[CommentTable]

  def create(post: Comment): Future[Unit] = {
    db run(comments += post) map(_ => ())
  }

  def delete(id: Long): Future[Int] = {
    db run(comments filter(_.id === id)).delete
  }

  def find(id: Long): Future[Option[Comment]] = {
    cache.getOrElse[Future[Option[Comment]]](id.toString, 30 seconds) {
      db run comments.filter(_.id === id).result.headOption
    }
  }

  def findAll: Future[Seq[Comment]] = {
    cache.getOrElse[Future[Seq[Comment]]]("All Comments", 1 hours) {
      db run comments.result
    }
  }

  def update(post: Comment): Future[Int] = {
    db run comments.filter(_.id === post.id).update(post)
  }

}

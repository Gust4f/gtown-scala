package model.daos

import javax.inject.Inject

import model.entities.{User, UserTable}
import org.joda.time.DateTime
import play.api.cache.CacheApi
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import slick.driver.JdbcProfile

import scala.concurrent.duration._
import scala.concurrent.Future

class UserDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                        protected val cache : CacheApi) extends HasDatabaseConfigProvider[JdbcProfile]{
  import driver.api._
  import com.github.tototoshi.slick.MySQLJodaSupport._
  private lazy val users = TableQuery[UserTable]

  def create(user: User): Future[Unit] = {
    db run(users += user) map(_ => ())
  }

  def delete(email: String): Future[Int] = {
    db run(users filter(_.email === email)).delete
  }

  def findByEmail(email: String): Future[Option[User]] = {
    db run users.filter(_.email === email).result.headOption
  }

  def findAll: Future[Seq[User]] = {
    cache.getOrElse[Future[Seq[User]]]("All Users", 1 minute) {
      db run users.result
    }
  }

  def findByGuestToken(token: String): Future[Option[User]] = {
    db run users.filter(_.referToken === token).result.headOption
  }

  def setSessionToken(email: String, token: String): Future[Int] = {
    val now = new DateTime()
    val query = for { u <- users if u.email === email } yield (u.sessionToken, u.sessionTokenExpiration)
    db run query.update((token, now.plusDays(5)))
  }

  def findBySessionToken(token: String): Future[Option[User]] = {
    db run users.filter(_.sessionToken === token).result.headOption
  }

  def update(user: User): Future[Int] = {
    db run users.filter(_.email === user.email).update(user)
  }

  def generateNewReferToken(email: String, token: String, expiration: DateTime): Future[Int] = {
    val query = for {
      user <- users
      if user.email === email
    } yield (user.referToken, user.referTokenExpiration)
    db run query.update(token, expiration)
  }

  def changePassword(email: String, password: String, changeDate: DateTime): Future[Int] = {
    val query = for {
      user <- users
      if user.email === email
    } yield (user.password, user.passwordChangeDate)
    db run query.update(password, changeDate)
  }
}

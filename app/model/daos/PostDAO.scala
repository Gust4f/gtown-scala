package model.daos

import javax.inject.Inject

import model.entities.{Post, PostTable, User, UserTable}
import org.joda.time.DateTime
import play.api.cache.CacheApi
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import slick.driver.JdbcProfile

import scala.concurrent.duration._
import scala.concurrent.Future

class PostDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                        protected val cache : CacheApi) extends HasDatabaseConfigProvider[JdbcProfile]{
  import driver.api._
  import com.github.tototoshi.slick.MySQLJodaSupport._

  private lazy val posts = TableQuery[PostTable]

  def create(post: Post): Future[Unit] = {
    db run(posts += post) map(_ => ())
  }

  def delete(id: Long): Future[Int] = {
    db run(posts filter(_.id === id)).delete
  }

  def find(id: Long): Future[Option[Post]] = {
    db run posts.filter(_.id === id).result.headOption
  }

  def findAll(category: Option[String]): Future[Seq[Post]] = {
    val cacheKey = if (category.isDefined) "All Posts" + category.get else "All Posts"
    cache.getOrElse[Future[Seq[Post]]](cacheKey, 1 hours) {
      category match {
        case Some(x) =>
          db run (for {
            post <- posts.filter(_.category === x)
          } yield post).sortBy(_.published.desc).result
        case None =>
          db run (for {
            post <- posts
          } yield post).sortBy(_.published.desc).result
      }

    }
  }

  def find(start: Long, end: Long, category: Option[String]): Future[Seq[Post]] = {
    val base = "Post" + start + " to " + end
    val cacheKey = if (category.isDefined) base + category.get else base
    cache.getOrElse[Future[Seq[Post]]](cacheKey, 1 hours) {
      category match {
        case Some(x) =>
          db run posts.filter(_.category === x).sortBy(_.published.desc).drop(start).take(end).result
        case None =>
          db run posts.sortBy(_.published.desc).drop(start).take(end).result
      }
    }
  }

  def update(post: Post): Future[Int] = {
    db run posts.filter(_.id === post.id).update(post)
  }
}

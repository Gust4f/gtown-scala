package services

import java.util.UUID
import javax.inject.{Inject, Singleton}

import model.daos.UserDAO
import model.entities.User
import model.util.{ChangePasswordDTO, Credentials}
import org.joda.time.DateTime
import org.mindrot.jbcrypt.BCrypt

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by gustaf on 2016-08-05.
  */
class UserService @Inject()(private val userDAO: UserDAO) {

  def create(user: User): Future[Unit] = {
    val now = new DateTime()
    userDAO create
      User(
        user.admin,
        user.email,
        now,
        BCrypt.hashpw(user.password, BCrypt.gensalt),
        now,
        Some(UUID.randomUUID().toString),
        now.plusDays(1),
        None,
        now
      )
  }

  def delete(credentials: Credentials): Future[Int] = {
    authenticate(credentials) flatMap {
      case Some(_) => userDAO delete credentials.email
      case None => Future.successful(0)
    }
  }

  def findByEmail(email: String): Future[Option[User]] = {
    userDAO findByEmail email
  }

  def authenticate(credentials: Credentials): Future[Option[User]] = {
    findByEmail(credentials.email) map {
      case Some(user) => if (BCrypt.checkpw(credentials.password, user.password)) Some(user) else None
      case None => None
    }
  }

  def findByGuestToken(token: String): Future[Option[User]] = {
    userDAO findByGuestToken token
  }

  def setSessionToken(email: String, token: String): Future[Int] = {
    userDAO setSessionToken(email, token)
  }

  def findBySessionToken(token: String): Future[Option[User]] = {
    userDAO findBySessionToken token
  }

  def findAll(): Future[Seq[User]] = {
    userDAO findAll
  }

  def update(user: User): Future[Int] = {
    userDAO update user
  }

  def generateNewReferToken(email: String): Future[Option[User]] = {
    val now = new DateTime()
    userDAO generateNewReferToken(email, UUID.randomUUID().toString, now.plusDays(1)) flatMap {
      case 1 => userDAO.findByEmail(email)
      case _ => Future.successful(None)
    }
  }

  def changePassword(changePasswordDTO: ChangePasswordDTO): Future[Option[User]] = {
    authenticate(Credentials(changePasswordDTO.email, changePasswordDTO.oldPassword)) flatMap {
      case Some(_) =>
        userDAO changePassword(changePasswordDTO.email, BCrypt.hashpw(changePasswordDTO.newPassword, BCrypt.gensalt), new DateTime()) flatMap {
          case 1 => userDAO.findByEmail(changePasswordDTO.email)
          case _ => Future.successful(None)
        }
      case None => Future.successful(None)
    }
  }
}

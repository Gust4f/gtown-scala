package services

import javax.inject.Inject

import model.daos.PostDAO
import model.entities.{Post, User}

import scala.concurrent.Future

/**
  * Created by gustaf on 2016-08-05.
  */
class PostService @Inject()(private val postDAO: PostDAO) {

  def create(post: Post): Future[Unit] = {
    postDAO.create(post)
  }

  def delete(id: Long): Future[Int] = {
    postDAO.delete(id)
  }

  def find(id: Long): Future[Option[Post]] = {
    postDAO.find(id)
  }

  def find(start: Long, end: Long, category: Option[String]): Future[Seq[Post]] = {
    postDAO.find(start, end, category)
  }

  def findAll(category: Option[String]): Future[Seq[Post]] = {
    postDAO.findAll(category)
  }

  def update(post: Post): Future[Int] = {
    postDAO.update(post)
  }
}

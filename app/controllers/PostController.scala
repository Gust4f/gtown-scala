package controllers

import javax.inject.{Inject, Singleton}

import authentication.{AdminAction, UserAction}
import model.entities.Post
import play.api.libs.json.{JsError, Json, Writes}
import play.api.mvc.{Action, BodyParsers, Controller}
import services.{PostService, UserService}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class PostController @Inject()(private val postService: PostService,
                               private val userService: UserService,
                               private val userAction: UserAction,
                               private val adminAction: AdminAction) extends Controller{

  import model.util.JSONConversions._

  def findAll(category: Option[String]) = userAction.async { implicit request =>
    postService findAll category map { res =>
      Ok(Json obj("posts" -> Json.toJson(res)))
    }
  }

  def findRange(start: Long, end: Long, category: Option[String]) = userAction.async { implicit request =>
    postService find(start, end, category) map { res =>
      Ok(Json obj("posts" -> Json.toJson(res)))
    }
  }

  def find(id: Long) = userAction.async { implicit request =>
    postService find id map {
      case Some(post) => Ok(Json obj("post" -> Json.toJson(post)))
      case None => noPost
    }
  }

  def delete(id: Long) = adminAction.async { implicit request =>
    postService delete id  map { res =>
      if (res > 0 ) Ok(Json obj("status" -> "ok", "rows" -> res))
      else noPost
    }
  }

  def update(id: Long) = adminAction.async(BodyParsers.parse.json) { implicit request =>
    val inputData = request.body.validate[Post]
    inputData fold (
      errors =>  {
        scala.concurrent.Future {
          BadRequest(Json obj("status" -> "ko", "message" -> JsError.toJson(errors)))
        }
      },
      data => {
        postService update data map { res =>
          if (res > 0) Ok(Json obj("status" -> "updated", "message" -> "post updated"))
          else noPost
        }
      })
  }

  def create = adminAction(BodyParsers.parse.json) { implicit request =>
    val inputData = request.body.validate[Post]
    inputData fold (
      errors =>  {
        BadRequest(Json obj("status" ->"ko", "message" -> JsError.toJson(errors)))
      },
      data => {
        postService.create(data)
        Created(Json obj("status" -> "created", "message" -> "post created"))
      })
  }

  private def noPost = {
    NotFound(Json obj("status" ->"not found", "message" -> "post could not be found"))
  }
}

package controllers

import java.util.UUID
import javax.inject.Inject

import model.util.Credentials
import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.json.{JsError, Json}
import play.api.mvc.{Action, BodyParsers, Controller, Request}
import services.UserService
import play.api.i18n.MessagesApi
import play.api.i18n.I18nSupport

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class AuthController @Inject()(private val userService: UserService,
                               val messagesApi: MessagesApi) extends Controller with I18nSupport {

  import model.util.JSONConversions._


  def login = Action { implicit request =>
    Ok(views.html.guest.login())
  }

  def loginPost = Action.async(parse.json) { implicit request =>
    val inputData = request.body.validate[Credentials]
    inputData fold (
      errors =>  {
        scala.concurrent.Future {
          BadRequest(Json obj("status" -> "Bad request", "message" -> JsError.toJson(errors)))
        }
      },
      credentials => {
        userService authenticate credentials flatMap {
          case Some(user) => {
            val token = UUID.randomUUID().toString
            userService setSessionToken(user.email, token) map { res =>
              Ok(Json obj("status" -> "Ok", "message" -> token)).withNewSession.withSession("sessionToken" -> token)
            }
          }
          case None => Future.successful(Unauthorized)
        }
      })
  }

  /**
    * Logout and clean the session.
    */
  def logout = Action {
    Redirect(routes.HomeController.index()).withNewSession.flashing(
      "success" -> "You've been logged out"
    )
  }
}

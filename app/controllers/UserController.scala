package controllers

import javax.inject.{Inject, Singleton}

import authentication.{AdminAction, UserAction}
import model.entities.User
import model.util.{ChangePasswordDTO, Credentials}
import play.api.libs.json._
import play.api.mvc._
import services.UserService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class UserController @Inject()(private val userService: UserService,
                               private val adminAction: AdminAction,
                               private val userAction: UserAction) extends Controller {

  import model.util.JSONConversions._

  def findAll = adminAction.async { implicit request =>
    userService findAll() map { res =>
      Ok(Json obj("users" -> Json.toJson(res)))
    }
  }

  def findByEmail = userAction.async { implicit request =>
    userService findByEmail request.user.email map {
      case Some(x) => Ok(Json obj ("status" -> "Ok", "user" -> Json.toJson(x)))
      case None => noUser
    }
  }

  def delete = userAction.async(BodyParsers.parse.json) { implicit request =>
    val inputData = request.body.validate[Credentials]
    inputData fold(
      errors => {
        scala.concurrent.Future {
          BadRequest(Json obj("status" -> "Bad request", "message" -> JsError.toJson(errors)))
        }
      },
      credentials => {
        userService delete credentials map { res =>
          if (res > 0 ) Ok(Json obj("status" -> "Ok", "message" -> (res + " rows removed")))
          else noUser
        }
      })
  }

  def update = userAction.async(BodyParsers.parse.json) { implicit request =>
    val inputData = request.body.validate[User]
    inputData fold(
      errors => {
        scala.concurrent.Future {
          BadRequest(Json obj("status" -> "Bad Request", "message" -> JsError.toJson(errors)))
        }
      },
      data => {
        userService update data map { res =>
          if (res > 0) Ok(Json obj("status" -> "Ok", "message" -> ("user '"+ data.email +"' updated")))
          else noUser
        }
      })
  }

  def generateReferToken = userAction.async { implicit request =>
    userService generateNewReferToken request.user.email map {
      case Some(user) => Ok(Json obj("status" -> "Ok", "user" -> Json.toJson(user)))
      case None => noUser
    }
  }

  def changePassword = userAction.async(BodyParsers.parse.json) { implicit request =>
    val inputData = request.body.validate[ChangePasswordDTO]
    inputData fold(
      errors => {
        scala.concurrent.Future {
          BadRequest(Json obj("status" -> "Bad request", "message" -> JsError.toJson(errors)))
        }
      },
      changePasswordDTO => {
        userService changePassword changePasswordDTO map {
          case Some(user) => Ok(Json obj("status" -> "Ok", "user" -> Json.toJson(user)))
          case None => noUser
        }
      })
  }

  def create = Action.async(BodyParsers.parse.json) { implicit request =>
    request.body.validate[User] fold(
      _ => {
        Future.successful(BadRequest(Json obj("status" ->"Bad request", "message" -> "Invalid input")))
      },
      valid => {
        userService findByGuestToken valid.referToken.get flatMap {
          case Some(user) =>
            if (user.referTokenExpiration.isAfterNow)
              createUser(valid)
            else
              Future.successful(BadRequest(Json obj("status" -> "Bad request", "message" -> "Invalid referral token")))
          case None => Future.successful(BadRequest(Json obj("status" -> "Bad request", "message" -> "Invalid referral token")))
        }
      })
  }

  private def createUser(user: User): Future[Result] = {
    userService create user map(_ => Created(Json obj("status" -> "Created", "message" -> ("User "+ user.email +" created")))
      ) recover {
      case e: Exception => BadRequest(Json obj("status" -> "Bad request", "message" -> "Could not create user", "exception" -> e.getMessage))
    }
  }
  /*
  def grantAdminAccess(email: String) = adminAction.async { implicit request =>
    userService findByEmail email flatMap {
      case Some(user) => userService update(
        User(true, user.email, user.password, user.registrationDate, user.referToken, user.tokenExpiration)
        ) map { res =>
        if (res > 0) Ok else BadRequest
      }
      case None => Future.successful(BadRequest)
    }
  }

  def removeAdminAccess(email: String) = adminAction.async { implicit request =>
    userService findByEmail email flatMap {
      case Some(user) => userService update(
        User(false, user.email, user.password, user.registrationDate, user.referToken, user.tokenExpiration)
        ) map { res =>
        if (res > 0) Ok else BadRequest
      }
      case None => Future.successful(BadRequest)
    }
  }
  */
  private def noUser = {
    NotFound(Json obj("status" ->"Not found", "message" -> "User could not be found"))
  }
}

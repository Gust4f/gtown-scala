package controllers

import javax.inject.{Inject, Singleton}

import authentication.UserAction
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.Environment
import play.api.mvc.{Action, Controller}

@Singleton
class HomeController @Inject()(private val env: Environment,
                               private val userAction: UserAction) extends Controller {

  val startTime = DateTime.now
  val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm")

  implicit lazy val isProd = env.isProd

  def index = Action {
    Ok(views.html.guest.index("System has been up since: " + formatter.print(startTime)))
  }

  def home = userAction { implicit request =>
    Ok(views.html.authenticated.home(request.user.email, request.user.admin))
  }
}

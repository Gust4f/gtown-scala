define([
    'angularAMD',
    'ErrorHandler'
], function (angularAMD) {
    'use strict';
    var app = angular.module('gtown', ['ngRoute', 'angularSpinner', 'ngSanitize']);
    app.config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/assets/templates/home.html'
            })
            .when('/projects', {
                templateUrl: '/assets/templates/todo.html'
            })
            .when('/pictures', {
                templateUrl: '/assets/templates/todo.html'
            })
            .when('/blog', angularAMD.route({
                templateUrl: '/assets/templates/blog.html',
                controller: 'BlogController'
            }))
            .when('/blog/post/:postId', angularAMD.route({
                templateUrl: '/assets/templates/post.html',
                controller: 'PostController'
            }))
            .when('/account', angularAMD.route({
                templateUrl: '/assets/templates/account.html',
                controller: 'AccountController'
            }))
            .otherwise({redirectTo: "/"});
    });
    return angularAMD.bootstrap(app);
});
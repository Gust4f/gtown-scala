$(document).ready(function initIndexPageJS() {
    window.setTimeout(function () {
        $(".se-pre-con").fadeOut("fast");
    }, 1000);

    $(function fullPageModule() {
        var $aboutCol1 = $('#about-col-1');
        var $aboutCol2 = $('#about-col-2');
        var $playButton = $('#play');
        var $typoMsg = $('#screen-size-info');
        var $vignette = $('#vignette');
        var $gtownScala = $('#gtown-scala-container');
        var $gtownJava = $('#gtown-java-container');
        var $gtownFooter = $('.gtown-jobs-container');

        var animateEntry = function (index, nextIndex, direction) {
            if (nextIndex === 2) {
                $aboutCol1.addClass('fadeInLeft');
                $aboutCol2.addClass('fadeInRight');
            }
            if (index === 2) {
                $aboutCol1.addClass('fadeOutLeft');
                $aboutCol2.addClass('fadeOutRight');
            }
            if (nextIndex === 3) {
                if ($playButton.find('span').is(':visible')) {
                    $playButton.addClass('fadeInLeft');
                    $vignette.addClass('fadeIn');
                } else {
                    $typoMsg.addClass('fadeInLeft');
                }
            }
            if (index === 3) {
                if ($playButton.find('span').is(':visible') || $playButton.css('opacity') == 1) {
                    $playButton.addClass('fadeOutLeft');
                } else if ($typoMsg.is(':visible')) {
                    $typoMsg.addClass('fadeOutLeft');
                } else {
                    $playButton.addClass('fadeIn');
                }
            }
            if (nextIndex === 4) {
                if (index === 1 || index === 2 || index === 3) {
                    $gtownScala.addClass('bounceInUp');
                } else {
                    $gtownScala.addClass('bounceInDown');
                }
            }
            if (nextIndex === 5) {
                if (index !== 6) {
                    $gtownJava.addClass('bounceInUp');
                } else {
                    $gtownJava.addClass('bounceInDown');
                }
            }
            if (nextIndex === 6) {
                $gtownFooter.addClass('bounceInUp');
            }
        };
        var restoreElements = function (anchorLink, index) {
            $aboutCol1.removeClass('fadeInLeft fadeOutLeft');
            $aboutCol2.removeClass('fadeInRight fadeOutRight');
            $playButton.removeClass('fadeInLeft fadeOutLeft');
            $typoMsg.removeClass('fadeInLeft fadeOutLeft fadeIn');
            $vignette.removeClass('fadeIn');
            $gtownJava.removeClass('bounceInUp bounceInDown');
            $gtownScala.removeClass('bounceInUp bounceInDown');
            $gtownFooter.removeClass('bounceInUp');
        };

        $('#fullpage').fullpage({

            //Navigation
            menu: '#menu',
            lockAnchors: false,
            anchors: ['hello', 'about-section', 'introvert-section', 'gtown-scala-section', 'gtown-java-section', 'job-section', 'footer'],
            navigation: true,
            navigationPosition: 'right',
            navigationTooltips: ['hello', 'me', 'introvert', 'gtown-scala', 'gtown-java', 'jobs', 'end'],
            showActiveTooltip: false,
            slidesNavigation: false,
            slidesNavPosition: 'right',

            //Scrolling
            css3: true,
            scrollingSpeed: 700,
            autoScrolling: (function () {
                var height = $('body').height();
                return height >= 768;
            })(),
            fitToSection: true,
            fitToSectionDelay: 1000,
            scrollBar: false,
            easing: 'easeInOutCubic',
            easingcss3: 'ease',
            loopBottom: false,
            loopTop: false,
            loopHorizontal: true,
            continuousVertical: false,
            continuousHorizontal: false,
            scrollHorizontally: false,
            interlockedSlides: false,
            dragAndMove: false,
            offsetSections: false,
            resetSliders: false,
            fadingEffect: false,
            normalScrollElements: '#element1, .element2',
            scrollOverflow: false,
            scrollOverflowOptions: null,
            touchSensitivity: 15,
            normalScrollElementTouchThreshold: 5,
            bigSectionsDestination: null,

            //Accessibility
            keyboardScrolling: true,
            animateAnchor: true,
            recordHistory: true,

            //Design
            controlArrows: true,
            verticalCentered: true,
            paddingTop: '3em',
            paddingBottom: '10px',
            fixedElements: '.modal',
            responsiveWidth: 0,
            responsiveHeight: 0,
            responsiveSlides: false,

            //Custom selectors
            sectionSelector: '.section',
            slideSelector: '.slide',

            lazyLoading: true,

            //events
            onLeave: animateEntry,
            afterLoad: restoreElements,
            afterRender: function () {
            },
            afterResize: function () {
            },
            afterResponsive: function (isResponsive) {
            },
            afterSlideLoad: function (anchorLink, index, slideAnchor, slideIndex) {
            },
            onSlideLeave: function (anchorLink, index, slideIndex, direction, nextSlideIndex) {
            }
        });
    });

    $(function footerModule() {
        var $modal = $('#foot-modal');
        var $dynamicfoot = $('div.dynamicfoot');
        var $label = $('#foot-modal-label');

        var _show = function show() {
            $modal.modal({
                show: true
            });
        };

        $('#termslink').on('click', function showTermsModal(event) {
            event.preventDefault();
            $dynamicfoot.html($('#terms').html());
            $label.html('Terms and Conditions of Use');
            _show();
        });

        $('#privacylink').on('click', function showPrivacyModal(event) {
            event.preventDefault();
            $dynamicfoot.html($('#policy').html());
            $label.html('Privacy Policy');
            _show();
        });
    });

    $(function terminalModule() {
        var greeting = ' ___________ \n' +
            '< Greetings > \n' +
            ' ----------- \n' +
            '        \\  ^__^\n' +
            '         \\ (oo)\\_______\n' +
            '           (__)\\       )\\/\\\n' +
            '                ||----w |\n' +
            '                ||     ||';
        var banner = '  ___________________\n' +
            ' /  _____/\\__    ___/_____  _  ______\n' +
            '/   \\  ___  |    | /  _ \\ \\/ \\/ /    \\\n' +
            '\\    \\_\\  \\ |    |(  (_) )     /   |  \\\n' +
            ' \\______  / |____| \\____/ \\/\\_/|___|  /\n' +
            '        \\/                          \\/\n' +
            "Use 'commands' to see available commands";
        var greetings = function (term) {
            term.echo(greeting);
        };
        var typed = function (msg, term) {
            if (msg.length === 0) {
                term.set_command('');
                greetings(term);
            } else {
                setTimeout(function () {
                    term.insert(msg[0]);
                    typed(msg.substring(1), term);
                }, 300);
            }
        };

        var login = function (username, password, term) {
            $.ajax({
                method: "POST",
                url: 'https://' + window.location.host + "/login",
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                data: JSON.stringify({
                    "email": username,
                    "password": password
                }),
                success: function (response) {
                    term.echo('[[b;green;]SUCCESS]: Redirecting to home page');
                    window.location.href = 'https://' + window.location.host + '/home';
                },
                error: function (response) {
                    term.echo('[[b;red;]FAILED]: Invalid login');
                }
            });
        };

        $('#term').terminal(function (command, term) {
            if (command === 'login') {
                var username, password;
                var history = term.history();
                history.disable();
                term.push(function (input) {
                    username = input;
                    term.pop();
                    term.set_mask(true);
                    term.push(function (input) {
                        password = input;
                        term.pop();
                        history.enable();
                        term.set_mask(false);
                        login(username, password, term);

                    }, {
                        prompt: 'Enter your password now: '
                    });
                }, {
                    prompt: 'Enter your username now: '
                });
            }
            else if (command === 'help') {
                term.echo(
                    'This terminal can navigate this site and ' +
                    'as an authenticated user there is a live ' +
                    'streaming service to access all media on ' +
                    'my drives anywhere. Otherwise the site is ' +
                    'mainly used as a playground to try out new ' +
                    'frameworks and tools. \n\n' +
                    'Thanks for showing interest in this demonstration site!'
                );
            }
            else if (command === 'commands') {
                term.echo('help, clear, greetings, guest, login');
            } else if (command === 'greetings') {
                greetings(term);
            } else if (command === 'guest') {
                term.echo(
                    'As a guest you may see the content on this ' +
                    'index page and if you have gotten a referral token ' +
                    'you will be able to use the entire site in full as ' +
                    'an authenticated guest. These tokens are timed and used in ' +
                    'custom guest sessions and an already registered user can ' +
                    'generate unique personal timed tokens. On share buttons as ' +
                    'an authenticated user the current valid token for the user will ' +
                    'be automatically appended to the output link. So when you use a ' +
                    'guest token your actions will be linked to the person who sent ' +
                    'you the token and you will be able to see the pages as if you are ' +
                    'the person who generated the guest token.'
                );
            } else {
                term.echo('Unknown command');
            }
        }, {
            greetings: (function () {
                var width = $('body').width();
                if (width > 380) {
                    return banner;
                }
                return '';
            })(),
            onInit: function (term) {
                typed('greetings', term);
            },
            onClear: function (term) {
                term.echo(banner);
            }
        });
    });

    $(function kineticTypographyModule() {

        /*
         Scenes
         */
        var $play = $('.play'),
            $vignette = $('.vignette'),
            scenes = $(".scenes").html(),
            $container = $('.introvert-presentation-section');

        /*
         Figure out if we are going to use this much JS or not.
         All timings and animations can be controlled from CSS but
         I am not advanced with CSS and I'm not really a designer..

         Current way to slay that that I'm thinking about is to maybe
         keep timings in JS and then I create custom classes with
         animations I just add my html. JS would then require a lot of
         promises and chaining. Currently I have found fonts and I have
         rendered the scenes I've made. They look good, now the transition
         and timing part.. i.e the hard part.

         I'm thinking of aligning everything in each scene as I want it
         and then I render the whole page as it would look before the
         transition to the next scene. So I have the layout and structure
         done and then I should probably just be able to add animations to
         that with classes timed using JS promises.
         */
        $play.on('click', function (event) {
            var $scene1 = $(".scene-1"),
                $scene2 = $(".scene-2"),
                $scene3 = $(".scene-3"),
                $scene4 = $("#scene-4"),
                $scene5 = $("#scene-5"),
                $scene6 = $("#scene-6"),
                $scene7 = $(".scene-7");


            event.preventDefault();

            var direct = function (actor, direction, delay) {
                var $actor = $(actor);
                setTimeout(function () {
                    direction.split(' ').forEach(function (elem) {
                        if ($actor.hasClass(elem)) {
                            $actor.removeClass(elem);
                        }
                        else {
                            $actor.addClass(elem);
                        }
                    });
                }, delay);
            };

            var reset = function (millis) {
                setTimeout(function () {
                    $(".scenes").html(scenes);
                    $container.removeClass('action');
                    $vignette.removeClass('fadeOut');
                    $play.removeClass('fadeOut hidden');
                    $vignette.addClass('fadeIn');
                    $play.addClass('fadeIn');
                    setTimeout(function () {
                        $vignette.removeClass('fadeIn');
                        $play.removeClass('fadeIn');
                    }, 1000);
                }, millis);
            };

            direct($vignette, 'fadeOut', 0);
            direct($play, 'fadeOut', 0);
            direct($play, 'hidden', 490);
            direct($container, 'action', 500);
            direct($scene1, 'action', 500);
            $scene3.find('.act-1').css('height', $container.height());
            $scene3.find('.act-2').css('height', $container.height());
            $scene3.find('.act-3').css('height', $container.height());
            $scene3.find('.act-4').css('height', $container.height());
            $scene7.find('.act-1').css('height', $container.height());
            direct($scene1, 'hello-world', 600);

            direct($scene1, 'cut', 3500);
            direct($scene1, 'exit', 4000);
            direct($scene2, 'action', 4200);
            direct($scene2.find('.iam-first'), 'action', 4200);
            direct($scene2.find('.iam-second'), 'action', 4400);
            direct($scene2.find('.signature'), 'action', 5000);
            direct($scene2.find('.i-am'), 'cut', 10000);
            direct($scene2.find('.signature'), 'cut', 10000);
            direct($scene2.find('.signature'), 'exit', 10000);
            direct($scene2.find('.i-am'), 'exit', 10200);
            direct($scene2, 'exit', 10500);

            // ------------------ END SCENE 2 ------------------

            direct($scene3, 'action', 10500);
            direct($scene3.find('.act-1 #act-1-part-1'), 'action', 10500);
            direct($scene3.find('.act-1 #act-1-part-2'), 'action', 10700);
            direct($scene3.find('.act-1 #act-1-part-2'), 'implode', 11000);
            direct($scene3.find('.act-1 #act-1-part-3'), 'action', 11400);
            direct($scene3.find('.act-1 #act-1-part-4'), 'action', 11800);
            direct($scene3.find('.act-1 #act-1-part-5'), 'action', 12200);
            direct($scene3.find('.act-1 #act-1-part-6'), 'action', 12600);
            direct($scene3.find('.act-1 #act-1-part-7'), 'action', 13000);
            direct($scene3.find('.act-1 #act-1-part-8'), 'action', 13400);
            direct($scene3.find('.act-1 #act-1-part-9'), 'action hidden', 13800);
            direct($scene3.find('.act-1 #act-1-part-10'), 'action hidden', 14200);
            direct($scene3.find('.act-1 #act-1-part-11'), 'action hidden', 14600);

            direct($scene3.find('.act-1 #act-1-part-12'), 'action hidden', 15000);
            direct($scene3.find('.act-1 #act-1-part-13'), 'action hidden', 15400);
            direct($scene3.find('.act-1 #act-1-part-14'), 'action hidden', 15800);
            direct($scene3.find('.act-1 #act-1-part-15'), 'action hidden', 16200);
            direct($scene3.find('.act-1 #act-1-part-16'), 'action hidden', 16600);
            direct($scene3.find('.act-1'), 'cut', 20000);
            direct($scene3.find('.act-1'), 'exit', 20200);

            direct($scene3.find('.act-2 .act-2-part-1'), 'action', 20400);
            direct($scene3.find('.act-2 .act-2-part-2'), 'action', 20500);
            direct($scene3.find('.act-2 .act-2-part-3'), 'action', 20600);
            direct($scene3.find('.act-2 .act-2-part-4'), 'action', 20800);
            direct($scene3.find('.act-2 .act-2-part-5'), 'action', 21000);
            direct($scene3.find('.act-2 .act-2-part-6'), 'action', 21200);

            direct($scene3.find('.act-2'), 'cut', 24500);
            direct($scene3.find('.act-2'), 'exit', 24700);

            direct($scene3.find('.act-3'), 'action', 25000);
            direct($scene3.find('.act-3 .act-3-part-1'), 'action', 25000);
            direct($scene3.find('.act-3 .act-3-part-2'), 'action', 25200);
            direct($scene3.find('.act-3 .act-3-part-2'), 'shine', 25400);
            direct($scene3.find('.act-3 .act-3-part-3'), 'action', 25400);
            direct($scene3.find('.act-3 .act-3-part-4'), 'action', 25600);
            direct($scene3.find('.act-3 .act-3-part-5'), 'action', 25800);
            direct($scene3.find('.act-3 .act-3-part-6'), 'action', 26000);
            direct($scene3.find('.act-3 .act-3-part-7'), 'action', 26200);
            direct($scene3.find('.act-3 .act-3-part-8'), 'action', 26400);
            direct($scene3.find('.act-3 .act-3-part-9'), 'action', 26400);

            $scene3.find('.act-3 .act-3-part-10 span').each(function (index, elem) {
                var delay = 26800 + (index * 50);
                direct($(this), 'action', delay);
            }); // Done at 5700 (14 characters / span elements)

            direct($scene3.find('.act-3'), 'cut', 30500);
            direct($scene3.find('.act-3'), 'exit', 30700);

            direct($scene3.find('.act-4 .act-4-part-1'), 'action', 31000);
            direct($scene3.find('.act-4 .act-4-part-2'), 'action', 31200);
            direct($scene3.find('.act-4 .act-4-part-3'), 'action', 31400);
            direct($scene3.find('.act-4 .act-4-container-2'), 'action', 32200);

            direct($scene3.find('.act-4'), 'cut', 40500);
            direct($scene3.find('.act-4'), 'exit', 40700);

            // ------------------ END SCENE 3 ------------------

            direct($scene7, 'action', 41000);

            direct($scene7.find('.act-1 .credits'), 'action', 41000);
            $scene7.find('.act-1 .special-thanks div').each(function (index, elem) {
                var entryDelay = 41000 + (index * 2000),
                    cutDelay = entryDelay + 1500,
                    exitDelay = cutDelay + 500;
                direct($(this), 'action', entryDelay);
                direct($(this), 'cut', cutDelay);
                direct($(this), 'exit', exitDelay);
            });
            direct($scene7.find('.act-1'), 'cut', 69500);
            direct($scene7.find('.act-1'), 'exit', 69000);
            direct($scene7.find('.act-final'), 'action', 69000);

            reset(69500);
        });


    });
});
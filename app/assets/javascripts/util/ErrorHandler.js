/*global define */

define([], function () {
    'use strict';
    var module = {};
    module.getErrorMsg = function (response) {
        if (response.status === -1) {
            return "The backend could not be reached";
        }
        if (response.data.message) {
            return response.data.message;
        }
        return response.message;
    };
    return module;
});

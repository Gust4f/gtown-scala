/*global define */


define(['app'], function (app) {
    app.factory('AccountService', ['$http', '$location', function ($http, $location) {
        'use strict';
        var service = {};
        var baseUrl = $location.$$protocol + '://' + $location.$$host + ":" + $location.$$port;

        service.get = function get() {
            var url = baseUrl + '/users/user';
            return $http.get(url);
        };

        service.deleteAccount = function deleteAccount(email, password) {
            return $http.post(baseUrl + '/users/user', {email: email, password: password});
        };

        service.update = function update(user) {
            return $http.put(baseUrl + '/users/user', user);
        };

        service.generateToken = function () {
            return $http.get(baseUrl + '/users/user/generateToken');
        };

        service.changePassword = function (email, oldPassword, newPassword) {
            return $http.post(baseUrl + '/users/user/changePassword', {
                email: email,
                oldPassword: oldPassword,
                newPassword: newPassword
            });
        };

        return service;
    }]);
});
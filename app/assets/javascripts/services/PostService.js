/*global define */


define(['app'], function (app) {
    app.factory('PostService', ['$http', '$location', function ($http, $location) {
        'use strict';
        var service = {};
        var paginationNr = 3;
        var baseUrl = $location.$$protocol + '://' + $location.$$host + ":" + $location.$$port;

        service.get = function get(postId) {
            var url = baseUrl + '/posts/post/' + postId + _getQueryParamString();
            return $http.get(url);
        };

        service.getAll = function getAll(page, category) {
            var start = page * paginationNr;
            var end = start + paginationNr;
            var url = baseUrl + '/posts/from/' + start + '/to/' + end + _getQueryParamString(category);
            return $http.get(url);
        };

        service.delete = function deletePost(postId) {
            return $http.delete(baseUrl + '/posts/post/' + postId);
        };

        service.create = function createPost(post) {
            return $http.post(baseUrl + '/posts/post', post);
        };

        service.update = function updatePost(post) {
            return $http.put(url, post);
        };

        var _getQueryParamString = function (category) {
            var abs = $location.absUrl();
            if (abs.indexOf('?') > -1) {
                if (category) {
                    return abs.substring(abs.indexOf('?'), abs.indexOf('#')) + '&category=' + category;
                }
                return abs.substring(abs.indexOf('?'), abs.indexOf('#'));
            } else {
                if (category) {
                    return '?category=' + category;
                }
                return '';
            }
        };

        return service;
    }]);
});
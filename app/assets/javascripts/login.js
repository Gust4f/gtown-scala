$(document).ready(function () {

    window.setTimeout(function () {
        $(".se-pre-con").fadeOut("fast");
    }, 1000);

    $('.flexslider').flexslider({
        directionNav: false,
        controlNav: false
    });

    (function loginModule($, window) {

        $('#login-form').on('submit', function (event) {
            event.preventDefault();
            var username = $('#username').val();
            var password = $('#password').val();
            var $usernameContainer = $('#username-container');
            var $passwordContainer = $('#password-container');
            var $usernameStatus = $('#username-status');
            var $passwordStatus = $('#password-status');
            if (username === '') {
                $usernameContainer.addClass('has-error has-feedback');
                $usernameStatus.text('Username is required');
                $usernameStatus.removeClass('sr-only');
                return;
            }
            $usernameContainer.removeClass('has-error has-feedback');
            $usernameStatus.addClass('sr-only');
            if (password === '') {
                $passwordContainer.addClass('has-error has-feedback');
                $passwordStatus.text('Password is required');
                $passwordStatus.removeClass('sr-only');
                return;
            }
            $.ajax({
                method: "POST",
                url: 'https://' + window.location.host + "/login",
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                data: JSON.stringify({
                    "email": username,
                    "password": password
                }),
                success: function (response) {
                    $usernameContainer.removeClass('has-error has-feedback');
                    $passwordContainer.removeClass('has-error has-feedback');
                    $usernameStatus.addClass('sr-only');
                    $passwordStatus.addClass('sr-only');
                    window.location.href = 'https://' + window.location.host + '/home';
                },
                error: function (response) {
                    var $errorAlert = $('#error-message');
                    $errorAlert.toggleClass('hidden');
                    if (response.status === 500) {
                        $errorAlert.text('There is a problem on the server');
                    } else if (response.status === 401) {
                        $errorAlert.text('Unknown account / wrong credentials');
                    }
                    $errorAlert.text(response.message);
                }
            });
        });
    })($, window);

    (function registrationModule($) {
        var $username = $('#username-reg');
        var $usernameContainer = $('#username-reg-container');
        var $usernameStatus = $('#username-reg-status');
        var $usernameX = $('#username-reg-x');

        var $password = $('#password-reg');
        var $passwordContainer = $('#password-reg-container');
        var $passwordStatus = $('#password-reg-status');
        var $passwordX = $('#password-reg-x');

        var $token = $('#token-reg');
        var $tokenContainer = $('#token-reg-container');
        var $tokenStatus = $('#token-reg-status');
        var $tokenX = $('#token-reg-x');

        var validateUserName = function (event) {
            var username = $username.val();
            var emailRegex = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
            if (!emailRegex.test(username)) {
                $usernameContainer.addClass('has-error has-feedback');
                $usernameStatus.text('Email is required');
                $usernameStatus.removeClass('sr-only');
                $usernameX.show();
                return;
            }
            $usernameContainer.removeClass('has-error has-feedback');
            $usernameStatus.addClass('sr-only');
            $usernameX.hide();
            return username;
        };

        var validatePassword = function (event) {
            var password = $password.val();
            var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})");
            if (!strongRegex.test(password)) {
                $passwordContainer.addClass('has-error has-feedback');
                $passwordStatus.text('6 char min length, upper, lower & number');
                $passwordStatus.removeClass('sr-only');
                $passwordX.show();
                return;
            }
            $passwordContainer.removeClass('has-error has-feedback');
            $passwordStatus.addClass('sr-only');
            $passwordX.hide();
            return password;
        };

        var validateToken = function (event) {
            var token = $token.val();
            if (token.length < 36) {
                $tokenContainer.addClass('has-error has-feedback');
                $tokenStatus.text('You must supply a token');
                $tokenStatus.removeClass('sr-only');
                $tokenX.show();
                return;
            }
            $tokenContainer.removeClass('has-error has-feedback');
            $tokenStatus.addClass('sr-only');
            $tokenX.hide();
            return token;
        };

        var resetInputFields = function () {
            $username.val('');
            $password.val('');
            $token.val('');

            $usernameContainer.removeClass('has-error has-feedback');
            $usernameStatus.addClass('sr-only');
            $usernameX.hide();

            $passwordContainer.removeClass('has-error has-feedback');
            $passwordStatus.addClass('sr-only');
            $passwordX.hide();

            $tokenContainer.removeClass('has-error has-feedback');
            $tokenStatus.addClass('sr-only');
            $tokenX.hide();
        };

        $('#username-reg').focusout(validateUserName);

        $('#password-reg').focusout(validatePassword);

        $('#token-reg').focusout(validateToken);

        $('#create-account').on('click', function (event) {
            var username = validateUserName();
            var password = validatePassword();
            var token = validateToken();

            if (username && password && token) {
                var now = new Date().getTime();
                $.ajax({
                    method: 'POST',
                    url: 'https://' + window.location.host + '/users/user/create',
                    data: JSON.stringify({
                        admin: false,
                        email: username,
                        passwordChangeDate: now,
                        password: password,
                        registrationDate: now,
                        referToken: token,
                        referTokenExpiration: now,
                        sessionToken: null,
                        sessionTokenExpiration: now
                    }),
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        var $success = $('#success-message');
                        $success.text(response.message);
                        $success.removeClass('hidden');
                        $('#registration-modal').modal('toggle');
                        resetInputFields();
                    },
                    error: function (response) {
                        var $errorAlert = $('#reg-error-message');
                        $errorAlert.toggleClass('hidden');
                        if (response.status === 500) {
                            $errorAlert.text('There is a problem on the server');
                        } else {
                            $errorAlert.text(response.responseJSON.message);
                        }
                    }
                });
            }
        });
    })($);
});
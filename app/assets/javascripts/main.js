/*global require, requirejs */

(function mainModule() {
    'use strict';
    requirejs.config({
        paths: {
            'jquery': [
                '/assets/lib/jquery/jquery.min',
                '/assets/lib/jquery/jquery'
            ],
            'angular': [
                '/assets/lib/angular/angular.min',
                '/assets/lib/angular/angular'
            ],
            'angularAMD': [
                '/assets/lib/angularAMD/angularAMD.min',
                '/assets/lib/angularAMD/angularAMD'
            ],
            'ngload': [
                '/assets/lib/angularAMD/ngload.min',
                '/assets/lib/angularAMD/ngload'
            ],
            'angular-route': [
                '/assets/lib/angular-route/angular-route.min',
                '/assets/lib/angular-route/angular-route'
            ],
            'spin': [
                '/assets/lib/spin.js/spin.min',
                '/assets/lib/spin.js/spin'
            ],
            'angular-spinner': [
                '/assets/lib/angular-spinner/angular-spinner.min',
                '/assets/lib/angular-spinner/angular-spinner'
            ],
            'angular-sanitize': [
                '/assets/lib/angular-sanitize/angular-sanitize.min',
                '/assets/lib/angular-sanitize/angular-sanitize'
            ],
            'app': '/assets/javascripts/app',
            'ErrorHandler': '/assets/javascripts/util/ErrorHandler',
            'PostService': '/assets/javascripts/services/PostService',
            'AccountService': '/assets/javascripts/services/AccountService',
            'HomeController': '/assets/javascripts/controllers/HomeController',
            'BlogController': '/assets/javascripts/controllers/BlogController',
            'PostController': '/assets/javascripts/controllers/PostController',
            'AccountController': '/assets/javascripts/controllers/AccountController'
        },
        shim: {
            'angular': {
                deps: ['jquery'],
                exports: 'angular'
            },
            'angular-route': {
                deps: ['angular']
            },
            'angular-sanitize': {
                deps: ['angular']
            },
            'angularAMD': {
                deps: ['angular']
            },
            'ngload': {
                deps: ['angularAMD']
            },
            'app': {
                deps: ['jquery', 'angular', 'angular-spinner', 'angularAMD', 'angular-route', 'angular-sanitize']
            }
        },
        deps: ['app']
    });
}).call(this);
/*global define */

define(['app', 'ErrorHandler', 'AccountService'], function (app, ErrorHandler) {
    'use strict';

    app.controller('AccountController', ['$rootScope', '$scope', '$location', '$routeParams', 'AccountService', 'usSpinnerService',
        function ($rootScope, $scope, $location, $routeParams, AccountService, usSpinnerService) {
            // Alert dialogs - activated and visible if not null
            $scope.error = null;
            $scope.info = null;
            $scope.success = null;
            // -------------------
            var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})");

            $scope.getUser = function () {
                var success = function (response) {
                    $scope.user = response.data.user;
                };
                AccountService.get($rootScope.email).then(success, failure);
            };

            $scope.updateUser = function () {
                var success = function (response) {
                    $scope.user = response.data.user;
                };
                AccountService.update($scope.user).then(success, failure);
            };

            var failure = function (response) {
                $scope.error = ErrorHandler.getErrorMsg(response);
                usSpinnerService.stop('spinner-1');
            };

            $scope.changePassword = function () {
                var success = function (response) {
                    $scope.user = response.data.user;
                    $scope.openPasswordModal = false;
                };
                if (!$scope.oldPassword) {
                    $scope.passwordError = 'You need to supply your old password!';
                }
                if (strongRegex.test($scope.newPassword)) {
                    if ($scope.newPassword === $scope.newPasswordX) {
                        AccountService.changePassword($scope.user.email, $scope.oldPassword, $scope.newPassword).then(success, failure);
                    } else {
                        $scope.passwordError = 'Passwords do not match';
                    }
                } else {
                    $scope.passwordError = '8 char min length, upper, lower & number';
                }
            };

            $scope.generateToken = function () {
                var success = function (response) {
                    $scope.user = response.data.user;
                };
                AccountService.generateToken().then(success, failure);
            };

            $scope.deleteAccount = function () {
                var success = function (response) {
                    window.location.href = '/index';
                };
                AccountService.deleteAccount($scope.user.email, $scope.password).then(success, failure);
            };

            $scope.getUser();
        }]);
});

/*global define */

define(['app', 'ErrorHandler', 'PostService'], function (app, ErrorHandler) {
    'use strict';

    app.controller('PostController', ['$scope', '$location', '$routeParams', 'PostService', 'usSpinnerService',
        function ($scope, $location, $routeParams, PostService, usSpinnerService) {
            // Alert dialogs - activated and visible if not null
            $scope.error = null;
            $scope.info = null;
            $scope.success = null;
            // -------------------

            $scope.postId = $routeParams.postId;

            $scope.getPost = function (postId) {
                var success = function (response) {
                    $scope.post = response.data.post;
                };
                PostService.get(postId).then(success, failure);
            };

            var failure = function (response) {
                $scope.error = ErrorHandler.getErrorMsg(response);
                usSpinnerService.stop('spinner-1');
            };

            $scope.getPost($scope.postId);
        }]);
});

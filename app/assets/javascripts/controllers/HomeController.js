/*global define */

define(['app', 'ErrorHandler', 'HomeService'], function (app, ErrorHandler) {
    'use strict';

    app.controller('HomeController', ['$scope', '$location', '$routeParams', 'HomeService', 'usSpinnerService',
        function ($scope, $location, $routeParams, UserService, usSpinnerService) {
            // Alert dialogs - activated and visible if not null
            $scope.error = null;
            $scope.info = null;
            $scope.success = null;
            // -------------------

            $scope.welcome = function () {

            };

        }]);
});

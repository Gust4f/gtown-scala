/*global define */

define(['app', 'ErrorHandler', 'PostService'], function (app, ErrorHandler) {
    'use strict';

    app.controller('BlogController', ['$scope', '$location', '$routeParams', 'PostService', 'usSpinnerService',
        function ($scope, $location, $routeParams, PostService, usSpinnerService) {
            // Alert dialogs - activated and visible if not null
            $scope.error = null;
            $scope.info = null;
            $scope.success = null;
            // -------------------
            $scope.posts = [];
            $scope.currentBlogPage = 0;

            $scope.getAll = function (page) {
                var success = function (response) {
                    $scope.posts.length = 0;
                    response.data.posts.forEach(function (elem) {
                        $scope.posts.push(elem);
                    });
                    usSpinnerService.stop('spinner-1');
                };
                usSpinnerService.spin('spinner-1');
                PostService.getAll(page, $scope.category).then(success, failure);
            };

            $scope.setCategory = function (category) {
                if ($scope.category === category) {
                    $scope.category = null;
                } else {
                    $scope.category = category;
                }
                $scope.getAll($scope.currentBlogPage);
            };

            $scope.next = function () {
                if ($scope.currentBlogPage !== 0) {
                    $scope.currentBlogPage -= 1;
                    $scope.getAll($scope.currentBlogPage);
                }
            };

            $scope.previous = function () {
                if ($scope.posts.length === 3) {
                    $scope.currentBlogPage += 1;
                    $scope.getAll($scope.currentBlogPage);
                }
            };

            var failure = function (response) {
                $scope.error = ErrorHandler.getErrorMsg(response);
                usSpinnerService.stop('spinner-1');
            };

            $scope.getAll($scope.currentBlogPage);
        }]);
});

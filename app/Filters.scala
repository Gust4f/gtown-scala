import javax.inject._

import play.api._
import play.api.http.{DefaultHttpFilters, HttpFilters}
import play.api.mvc._
import filters.{GuestFilter, RequestLoggingFilter, TLSFilter}
import play.filters.cors.CORSFilter
import play.filters.headers.SecurityHeadersFilter

/**
  * This class configures filters that run on every request. This
  * class is queried by Play to get a list of filters.
  *
  * Play will automatically use filters from any class called
  * `Filters` that is placed the root package. You can load filters
  * from a different class by adding a `play.http.filters` setting to
  * the `application.conf` configuration file.
  *
  * @param env Basic environment settings for the current application.
  * @param guestFilter adds a guest header if needed.
  * @param tlsFilter enforces HTTPS for all connections.
  * @param requestLoggingFilter logs all requests.
  *
  */
@Singleton
class Filters @Inject() (env: Environment,
                         guestFilter: GuestFilter,
                         tlsFilter: TLSFilter,
                         requestLoggingFilter: RequestLoggingFilter,
                         securityHeadersFilter: SecurityHeadersFilter,
                         corsFilter: CORSFilter)
  extends DefaultHttpFilters(requestLoggingFilter, guestFilter) {}

package authentication

import model.entities.User
import play.api.mvc.{ActionBuilder, Request, Result, WrappedRequest}

import scala.concurrent.Future

case class UserRequest[A](user: User, request: Request[A]) extends WrappedRequest[A](request)

trait SecurityTrait extends ActionBuilder[UserRequest] {
  override def invokeBlock[A](request: Request[A], block: (UserRequest[A]) => Future[Result]): Future[Result]
}

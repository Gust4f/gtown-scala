package authentication

import javax.inject.Inject

import play.api.mvc.Results._
import play.api.mvc.{Request, Result}
import services.UserService
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future

class AdminAction @Inject()(private val userService: UserService) extends SecurityTrait {
  override def invokeBlock[A](request: Request[A], block: (UserRequest[A]) => Future[Result]): Future[Result] = {
    lazy val sessionUser = request.session.get("user")
    sessionUser match {
      case Some(userEmail) => userService findByEmail userEmail flatMap {
        case Some(user) =>
          if (user.admin)
            block(UserRequest(user, request))
          else
            Future.successful(Forbidden)
        case None => Future.successful(Forbidden)
      }
      case None => Future.successful(Forbidden)
    }
  }
}

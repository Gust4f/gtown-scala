package authentication

import javax.inject.Inject

import play.api.libs.json.Json
import play.api.mvc.Results._
import play.api.mvc.{Request, Result}
import services.UserService

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class UserAction @Inject()(private val userService: UserService) extends SecurityTrait {
  val guest = "?guest="
  val guestHeader = "X-GUEST"

  override def invokeBlock[A](request: Request[A], block: (UserRequest[A]) => Future[Result]): Future[Result] = {
    if (request.uri.contains(guest)) {
      val uri = request.uri
      val beginIndex = uri.indexOf(guest)+guest.length
      val guestToken = uri.substring(beginIndex, beginIndex + 36)
      authenticateAsGuest(request, block, guestToken)
    } else {
      request.headers.get(guestHeader) match {
        case Some(token) => authenticateAsGuest(request, block, token)
        case None => authenticateAsUser(request, block)
      }
    }
  }

  private def authenticateAsGuest[A](request: Request[A], block: (UserRequest[A]) => Future[Result], token: String): Future[Result] = {
    userService findByGuestToken token flatMap {
      case Some(user) =>
        if (user.referTokenExpiration.isAfterNow)
          block(UserRequest(user, request))
        else
          Future.successful(Forbidden(Json obj("status" -> "Forbidden", "message" -> "That guest token has expired")))
      case None => Future.successful(Forbidden(Json obj("status" -> "Forbidden", "message" -> "That is an invalid guest token")))
    }
  }

  private def authenticateAsUser[A](request: Request[A], block: (UserRequest[A]) => Future[Result]): Future[Result] = {
    request.session.get("sessionToken") match {
      case Some(token) => userService findBySessionToken token flatMap {
        case Some(user) =>
          if (user.sessionTokenExpiration.isAfterNow)
            block(UserRequest(user, request))
          else
            Future.successful(Forbidden(Json obj("status" -> "Forbidden", "message" -> "Your session is invalid")))
        case None => Future.successful(Forbidden(Json obj("status" -> "Forbidden", "message" -> "Your session is invalid")))
      }
      case None => Future.successful(Forbidden(Json obj("status" -> "Forbidden", "message" -> "Please login to access this page")))
    }
  }
}

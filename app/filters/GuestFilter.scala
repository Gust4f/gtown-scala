package filters

import akka.stream.Materializer
import javax.inject._

import play.api.mvc._
import play.api.mvc.Results._
import services.UserService

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class GuestFilter @Inject()(implicit override val mat: Materializer,
                            private val exec: ExecutionContext) extends Filter {
  val guest = "?guest="
  val guestHeader = "X-GUEST"

  override def apply(f: (RequestHeader) => Future[Result])(rh: RequestHeader): Future[Result] = {
    f(rh) map { implicit result =>
      if (rh.uri.contains(guest) && rh.headers.get(guestHeader).isEmpty) {
        val uri = rh.uri
        val beginIndex = uri.indexOf(guest)+guest.length
        val guestToken = uri.substring(beginIndex, beginIndex + 36)
        result.withHeaders(guestHeader -> guestToken)
      } else result
    }
  }
}

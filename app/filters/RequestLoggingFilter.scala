package filters

import java.util.Calendar
import javax.inject.{Inject, Singleton}

import akka.stream.Materializer
import play.api.Logger
import play.api.libs.json.{JsArray, JsObject, Json}
import play.api.mvc.{Filter, RequestHeader, Result}
import play.api.routing.Router.Tags
import services.UserService

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class RequestLoggingFilter @Inject()(
                                      implicit override val mat: Materializer,
                                      private val userService: UserService,
                                      private val exec: ExecutionContext) extends Filter {

  override def apply(f: (RequestHeader) => Future[Result])(rh: RequestHeader): Future[Result] = {
    val startTime = System.currentTimeMillis

    f(rh).map { result =>
      val endTime = System.currentTimeMillis
      val requestTime = endTime - startTime
      val address = rh.remoteAddress
      val path = rh.path
      val method = rh.method
      val calendar = Calendar.getInstance()

      val log = Json obj (
        "date" -> calendar.getTime,
        "timezone" -> calendar.getTimeZone.getID,
        "method" -> method,
        "path" -> path,
        "requestTime" -> requestTime,
        "result" -> result.header.status,
        "address" -> address,
        "headers" -> getHeaders(rh.headers.headers)
        )
      Logger.info(log.toString())
      result.withHeaders("Request-Time" -> requestTime.toString)
    }
  }

  private def getHeaders(headers: Seq[(String, String)]): JsArray = {
    if (headers.isEmpty) Json arr()
    else Json.arr(Json.obj(headers.head._1 -> headers.head._2)) ++ getHeaders(headers.tail)
  }
}

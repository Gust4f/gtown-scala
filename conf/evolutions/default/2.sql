# --- !Ups
INSERT INTO USERS (EMAIL, PASSWORD)
VALUES ('zeus@olympus.com', '');

INSERT INTO POSTS (PUBLISHED, HEADER, BODY, FOOTER, CATEGORY, URL, AUTHOR)
VALUES (
  CURRENT_TIMESTAMP,
  'Automatically Generated Post1',
  '<p class="column_4 align text  justify">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex blanditiis voluptas voluptatum quod. Accusantium, molestiae impedit aliquam architecto velit ipsam laudantium nulla incidunt est praesentium optio eius error! Corporis, possimus!
    <br>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex blanditiis voluptas voluptatum quod. Accusantium, molestiae impedit aliquam architecto velit ipsam laudantium nulla incidunt est praesentium optio eius error! Corporis, possimus!
    <br>
  </p>

  <div class="column_5 img h256 margin-bottom"></div>
  <p class="column_9">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, dolorum, placeat, nobis molestiae eum dolor incidunt id nesciunt quas porro quaerat laboriosam assumenda voluptate ipsam cupiditate nam rem obcaecati error?
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, dolorum, placeat, nobis molestiae eum dolor incidunt id nesciunt quas porro quaerat laboriosam assumenda voluptate ipsam cupiditate nam rem obcaecati error?
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, dolorum, placeat, nobis molestiae eum dolor incidunt id nesciunt quas porro quaerat laboriosam assumenda voluptate ipsam cupiditate nam rem obcaecati error?
  </p>',
  'TEST FOOTER',
  'News',
  'TESTURL1',
  'zeus@olympus.com');

INSERT INTO POSTS (PUBLISHED, HEADER, BODY, FOOTER, CATEGORY, URL, AUTHOR)
VALUES (
  CURRENT_TIMESTAMP,
  'Automatically Generated Post2',
  '<p class="column_4 align text  justify">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex blanditiis voluptas voluptatum quod. Accusantium, molestiae impedit aliquam architecto velit ipsam laudantium nulla incidunt est praesentium optio eius error! Corporis, possimus!
    <br>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex blanditiis voluptas voluptatum quod. Accusantium, molestiae impedit aliquam architecto velit ipsam laudantium nulla incidunt est praesentium optio eius error! Corporis, possimus!
    <br>
  </p>

  <div class="column_5 img h256 margin-bottom"></div>
  <p class="column_9">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, dolorum, placeat, nobis molestiae eum dolor incidunt id nesciunt quas porro quaerat laboriosam assumenda voluptate ipsam cupiditate nam rem obcaecati error?
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, dolorum, placeat, nobis molestiae eum dolor incidunt id nesciunt quas porro quaerat laboriosam assumenda voluptate ipsam cupiditate nam rem obcaecati error?
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, dolorum, placeat, nobis molestiae eum dolor incidunt id nesciunt quas porro quaerat laboriosam assumenda voluptate ipsam cupiditate nam rem obcaecati error?
  </p>',
  'TEST FOOTER',
  'Fun',
  'TESTURL2',
  'zeus@olympus.com');

INSERT INTO POSTS (PUBLISHED, HEADER, BODY, FOOTER, CATEGORY, URL, AUTHOR)
VALUES (
  CURRENT_TIMESTAMP,
  'Automatically Generated Post3',
  '<p class="column_4 align text  justify">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex blanditiis voluptas voluptatum quod. Accusantium, molestiae impedit aliquam architecto velit ipsam laudantium nulla incidunt est praesentium optio eius error! Corporis, possimus!
    <br>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex blanditiis voluptas voluptatum quod. Accusantium, molestiae impedit aliquam architecto velit ipsam laudantium nulla incidunt est praesentium optio eius error! Corporis, possimus!
    <br>
  </p>

  <div class="column_5 img h256 margin-bottom"></div>
  <p class="column_9">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, dolorum, placeat, nobis molestiae eum dolor incidunt id nesciunt quas porro quaerat laboriosam assumenda voluptate ipsam cupiditate nam rem obcaecati error?
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, dolorum, placeat, nobis molestiae eum dolor incidunt id nesciunt quas porro quaerat laboriosam assumenda voluptate ipsam cupiditate nam rem obcaecati error?
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, dolorum, placeat, nobis molestiae eum dolor incidunt id nesciunt quas porro quaerat laboriosam assumenda voluptate ipsam cupiditate nam rem obcaecati error?
  </p>',
  'TEST FOOTER',
  'News',
  'TESTURL3',
  'zeus@olympus.com');

INSERT INTO COMMENTS (PUBLISHED, BODY, AUTHOR, POST_ID)
VALUES (
  CURRENT_TIMESTAMP,
  'TEST COMMENT',
  'zeus@olympus.com',
  (SELECT ID FROM POSTS WHERE URL = 'TESTURL1')
);

# --- !Downs
DELETE FROM USERS WHERE EMAIL = 'zeus@olympus.com';

DELETE FROM POSTS WHERE URL = 'TESTURL';

DELETE FROM COMMENTS WHERE BODY = 'TEST COMMENT';